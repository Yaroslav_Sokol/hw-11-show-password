const passwordInputs = document.querySelectorAll('input[type="password"]');
const passwordIcons = document.querySelectorAll('.icon-password');

passwordIcons.forEach((icon, index) => {
  icon.addEventListener('click', () => {
    const inputType = passwordInputs[index].type;

    if (inputType === 'password') {
      passwordInputs[index].type = 'text';
      icon.classList.remove('fa-eye');
      icon.classList.add('fa-eye-slash');
    } else {
      passwordInputs[index].type = 'password';
      icon.classList.remove('fa-eye-slash');
      icon.classList.add('fa-eye');
    }
  });
});

const form = document.querySelector('.password-form');
const submitBtn = document.querySelector('.btn');

function checkPasswords() {
  if (passwordInputs[0].value === passwordInputs[1].value) {
    alert('You are welcome');
  } else {
    const errorText = document.createElement('span');
    errorText.innerText = 'Потрібно ввести однакові значення';
    errorText.style.color = 'red';
    const inputWrapper = document.querySelectorAll('.input-wrapper')[1];
    inputWrapper.appendChild(errorText);
  }
}

submitBtn.addEventListener('click', function(event) {
  event.preventDefault();
  checkPasswords();
});
